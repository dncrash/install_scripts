#!/bin/bash

# test if root
##############
if [[ $(whoami) != 'root' ]];then
    echo 'this script must be ran as root. use "sudo -i"'
    exit
fi

# partition & format
####################
emerge parted
emerge btrfs-progs
fdisk -l
sure='false'
while [[ ${sure,,} != 'yes' && ${sure,,} != 'y' ]]; do
    read -p 'enter the disk you want to use for installation (ex: sda): ' disk
    if [[ -e /dev/$disk ]]; then
        read -p "$disk selected. are you sure (yes/no): " sure
    else
        echo "/dev/$disk does not exist. try again"
    fi
done
parted /dev/"$disk" mklabel gpt
parted /dev/"$disk" mkpart P1 1MiB 512MiB
parted /dev/"$disk" set 1 boot on
parted /dev/"$disk" mkpart P2 512MiB 96%
mkfs.vfat -F32 /dev/${disk}1
mkfs.btrfs -f /dev/${disk}2
mount /dev/${disk}2 /mnt/gentoo
mkdir /mnt/gentoo/boot
mount /dev/${disk}1 /mnt/gentoo/boot

# download stage 3 and extract
##############################
cd /mnt/gentoo
wget http://mirrors.evowise.com/gentoo/releases/amd64/autobuilds/latest-stage3-amd64.txt
latest=$(tail -1 latest-stage3-amd64.txt | cut -f 1 -d ' ')
wget http://mirrors.evowise.com/gentoo/releases/amd64/autobuilds/$latest
tar xvjpf stage3*.tar.bz2 --xattrs --numeric-owner
rm latest-stage3-amd64.txt
rm stage3*.tar.bz2

# set up repos
##############
# vim /etc/portage/make.conf
mirrorselect -i -o >> etc/portage/make.conf
mkdir /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
cat /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

# mount & copy files
####################
mount -t proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
cp -L /etc/resolv.conf /mnt/gentoo/etc

# chroot
########
cat <<EOF > /mnt/gentoo/root/chroot_script.sh
#!/bin/bash
PS1="(chroot) $PS1"
emerge-webrsync
# eselect profile list - see available profiles
eselect profile set 7 # (plasma + systemd)
emerge --update --deep --newuse @world

echo 'Europe/Bucharest' > /etc/timezone
emerge --config sys-libs/timezone-data
sed -i 's/#en_US\.UTF-8/en_US\.UTF-8/' /etc/locale.gen
locale-gen
eselect locale list
# might need to grep for it ?!
eselect locale set 3
env-update && source /etc/profile && export PS1="(chroot) $PS1"
emerge sys-kernel/gentoo-sources
# test by checking /usr/src/linux exists and is symlink
emerge sys-apps/pciutils
cd /usr/src/linux

# for manual kernel:
# make menuconfig # manual kernel config here: saves '.config' file
# make && make modules_install
# make install
# emerge sys-kernel/genkernel
# genkernel --install initramfs

nano /etc/fstab # add entries

emerge sys-kernel/genkernel
emerge sys-fs/btrfs-progs # otherwise genkernel will fail

genkernel all
# WARNING: Additional kernel cmdline arguments that *may* be required to boot properly...
# dobtrfs - btrfs device scanning support
# may be needed to add rootfstype=btrfs to the list of boot parameters !?
# according to wiki: add modules to /etc/conf.d/modules (modules="<modules>")
emerge sys-kernel/linux-firmware

read -p "enter hostname: " hostname
echo "hostname=$hostname" > /etc/conf.d/hostname

# network
#########
# emerge --norpeplace net-misc/netifrc # if package is missing
# emerge net-misc/dhcpcd # if missing
echo 'configure network via an interface running DHCP or manually'
read -p "configure network? (y/n): " network
if [[ ${network,,} == 'y' || ${network,,} == 'yes' ]]; then
    ip a
    read -p "enter network interface: " interface
    echo config_$interface="dhcp" > /etc/conf.d/net
    cd /etc/init.d
    ln -s net.lo net.$interface
    rc-update add net.$interface default
fi
passwd

read -p 'install GRUB? (y/n): ' grub
if [[ $grub ]]; then
    emerge --verbose sys-boot/grub:2
    grub-install --target=x86_64-efi --efi-directory=/boot
    grub-mkconfig -o /boot/grub/grub.cfg
fi
EOF

chroot /mnt/gentoo chmod +x /root/chroot_script.sh
chroot /mnt/gentoo /root/chroot_script.sh


# DONE
echo 'Finished setup. Ready for reboot.'
