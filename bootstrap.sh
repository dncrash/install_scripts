#!/bin/bash

set -eux
# 1. wireless

# cp /etc/netctl/examples/wireless-wpa /etc/netctl/wifi
# netctl edit wifi
read -p "wifi network name: " name
read -p "wifi ssid: " ssid
read -p "wifi pass: " pass

cat << EOF > /etc/netctl/"$name"
Description='A simple WPA encrypted wireless connection'
Interface=wlan0
Connection=wireless

Security=wpa
IP=dhcp

ESSID="$ssid"
# Prepend hexadecimal keys with \"
# If your key starts with ", write it as '""<key>"'
# See also: the section on special quoting rules in netctl.profile(5)
Key="$pass"
# Uncomment this if your ssid is hidden
#Hidden=yes
# Set a priority for automatic profile selection
#Priority=10
EOF

netctl start "$name"
while true; do 
    ping 8.8.8.8 -c 1 && break || ( echo 'no connectivity yet' && sleep 1 )
done

# 2. pkgs
pacman -Sy && pacman -S emacs-nox
