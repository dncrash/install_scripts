#!/bin/bash

set -eu

test_EFI() {
    if [[ $(ls -l /sys/firmware/efi/efivars 2>/dev/null | wc -l) -eq 0 ]]; then
        echo -e "\\e[31mcouldn't find EFI variable in /sys/firmware/efi/efivars\\e[0m"
        echo -e 'script requires EFI due to systemd boot'
        exit
    fi
}

setup_network() {
    echo -e 'basic DHCP wireless(WPA) or ethernet setup'
    echo -e "for fancy stuff press 'n' and diy"
    while true; do
        read -rp 'configure network (y/n): ' network
        [[ "$network" == 'y' || "$network" == 'n' ]] && break
    done
    if [[ "$network" == 'y' ]]; then
        interfaces=$(ls /sys/class/net)
        echo -e "$interfaces"
        while true; do
            read -rp 'select network interface from above: ' interface
            [[ "$interfaces" =~ $interface ]] && break
            echo -e "please select from $interfaces"
        done
        while true; do
            read -rp 'input w for wireless or e for ethernet: ' woe
            echo
            [[ "$woe" = 'w' || "$woe" = 'e' ]] && break
        done
        if [[ "$woe" == "w" ]]; then
            read -rp "wifi SSID: " SSID
            read -rp "wifi pass: " WIFI_PASS
            cat << EOF > /etc/netctl/wireless
Description='wireless-wpa'
Interface="$interface"
Connection=wireless
Security=wpa
IP=dhcp
ESSID="$SSID"
Key="$WIFI_PASS"
EOF

            netctl enable wireless && netctl start wireless

        else
            cat << EOF > /etc/netctl/ethernet
Description='ethernet dhcp on $interface'
Interface="$interface"
Connection=ethernet
IP=dhcp
EOF

            netctl enable ethernet && netctl start ethernet

        fi
    fi
    ping -i 0.5 -c 3 www.archlinux.org &> /dev/null && echo -e '\e[32mpinged archlinux.org to test connectivity\e[0m' || echo -e '\e[31mpinging archlinux.org test failed. please check network setup\e[0m'

}

encrypt_disk() {
    while true; do
        read -rp "encrypt disk? (y/n): " encrypt
        [[ "$encrypt" == 'y' || "$encrypt" == 'n' ]] && break
    done
    if [[ "$encrypt" == 'y' ]]; then
        cryptsetup -s 512 luksFormat "/dev/${disk}2"  # (assuming /dev/sda1 is /boot)
        cryptsetup open --type luks "/dev/${disk}2" cryptroot
        root='/dev/mapper/cryptroot'
    else
        root="/dev/${disk}2"
    fi

}

format_and_mount() {
    echo -e '\n formatting and mounting partitions \n'
    # root
    mkfs.btrfs -f "$root"
    mount "$root" /mnt
    # boot
    mkfs.vfat -F32 "/dev/${disk}1"
    mkdir /mnt/boot
    mount "/dev/${disk}1" /mnt/boot

}

partition_disk() {
    fdisk -l
    echo -e '\n'

    echo -e '\e[31mWARNING! CONFIGURING THE DISK USING THE SCRIPT REQUIRES NO LVM/RAID BE INSTALLED'
    echo -e 'WARNING! AUTOMATIC PARTITIONING WILL ERASE ALL DATA ON THE SELECTED DISK!\e[0m'
    echo -e 'selecting manual partitioning will drop you in \e[32mparted\e[0m and you must mount the partitions yourself in /mnt'
    while true; do
        read -rp "configure disk automatically (512MiB boot; 96% /; no swap) or manually (a/m): " partition
        [[ "$partition" == 'a' || "$partition" == 'm' ]] && break
        echo -e 'please insert a for automatic partitioning or m for manual'
    done
    if [[ "$partition" == 'm' ]]; then
        parted
        echo -e '\e[33mWARNING! You must mount / in /mnt and /boot in /mnt/boot\e[0m'
        while true; do
            read -rp "type 'continue' to resume installation after formatting and mounting the partitions: " cont
            [[ "$cont" == 'continue' ]] && break
        done
    else
        disks=$(lsblk | awk '/^sd\w/ {print $1}')
        echo -e "$disks"
        while true; do
            read -rp 'please select the disk you want to install to (ex: sda): ' disk
            if [[ "$disks" =~ $disk ]]; then
                echo -e "using $disk"
                break
            else
                echo -e "options are $disks"
            fi
        done
        parted /dev/"$disk" mklabel gpt
        parted /dev/"$disk" mkpart P1 1MiB 512MiB
        parted /dev/"$disk" set 1 boot on
        parted /dev/"$disk" mkpart P2 512MiB 96%

    fi

}

install_packages() {
    echo -e '\n updating pacman keys \n'
    pacman-key --refresh-keys
    echo -e 'activating ntpd. incorrect time may interfere with package installation'
    timedatectl set-ntp true
    echo -e '\n installing packages \n'
    echo -e "press 'Enter' to continue without installing additional packages"
    while true; do
        read -rp "additional packages list(ex: /root/pkglist.txt): " pkglist
        [[  -e "$pkglist"  || "$pkglist" == '' ]] && break
    done
    if [ -f "$pkglist" ]; then
        pacstrap /mnt base base-devel - < "$pkglist"
    else
        pacstrap /mnt base base-devel iw wpa_supplicant
    fi
    # intel u-code
    grep -qi intel /proc/cpuinfo && pacstrap /mnt intel-ucode

}

chroot_setup() {
    echo -e 'running chroot setup. (time, locale, hostname, bootloader)'
    cat << 'CHROOT' > /mnt/root/chroot_script.sh
#!/bin/bash
ln -sf /usr/share/zoneinfo/Europe/Bucharest /etc/localtime
hwclock --systohc
echo -e 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo -e 'LANG=en_US.UTF-8' > /etc/locale.conf
read -rp 'hostname: ' hostname
echo -e "$hostname" > /etc/hostname
echo -e 'set root password'
passwd
bootctl --path=/boot install
cat << EOF > /boot/loader/loader.conf
default arch
timeout 4
EOF
part_id=$(blkid -o export /dev/INSTALLDISK | grep PARTUUID)
if [[ -e /dev/mapper/cryptroot ]]; then
    opt_line="options cryptdevice=${part_id}:cryptroot root=/dev/mapper/cryptroot quiet rw"
    sed -i 's/udev/udev encrypt/' /etc/mkinitcpio.conf # find more elegant method
    mkinitcpio -p linux
else
    opt_line="options root=${part_id} quiet rw"
fi
cat << EOF > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd  /initramfs-linux.img
$opt_line
EOF
CHROOT

    sed -i "s/INSTALLDISK/${disk}2/" /mnt/root/chroot_script.sh # add real disk used during installation in chroot_script.sh
    chmod +x /mnt/root/chroot_script.sh
    arch-chroot /mnt /root/chroot_script.sh
    genfstab -U /mnt >> /mnt/etc/fstab # added here because /etc/fstab doesn't exist before installing the base system

}

finish() {
    rm /mnt/root/chroot_script.sh
    umount -R /mnt
    [ -e /dev/mapper/cryptroot ] && cryptsetup close cryptroot
    echo -e 'Finished setup. Unmounted disk. Ready for reboot.'
}

main() {
    test_EFI
    setup_network
    partition_disk
    encrypt_disk
    format_and_mount
    install_packages
    chroot_setup
    finish

}

main
